# ITWD.408

Advanced Internet and Web Page Development (Sept 2022) Final Project

## Review
---------------------
### Resolved Problems:
* Responsive web layout
* Integration of raw CSS flex-box, grid, and bootstrap
### Unresolved Problems:
### Network Publishing:
* Noticed that GitLab has a much better connection than GitHub, so I uploaded all my files on GitLab using git.
* bought a domain on Alibaba cloud
* but failed to deploy/publish the webpage
* It needs to use gitlab runner which I was stuck with.
### What could have been done:
* Always-on-top navigation bar
* More transitioning animation
* More consistent UI/UX
### What I learned:
* Built a website from raw HTML/CSS/JS
* the use of different css libraries
* the pros and cons of using frameworks
### Tips for the future:
* W3School has some great demos and exercises.
* Materialize, MUI, Element UI, uview, Vant are also worth looking into

## Developer

[Yining (Jenny) Chen ](cyining@126.com)

## License

This project is licensed under the [ZYU-EIT](https://zyu.eit.ac.nz/) License 

## Acknowledgments

Inspiration, code snippets, etc.
* [awesome-readme](https://github.com/matiassingers/awesome-readme)
* etc.
