import '@babel/polyfill';

class Booking extends React.Component {
  constructor(props) {
    super(props);
    //Properties
    this.state = {
      customerType: "consumer",
      gender: "",
      firstname: "",
      lastname: "",
      street: "",
      suburb: "",
      city: "",
      postcode: "",
      phone: "",
      email: "",
      
      purchase: "",
      repair: "",
      warranty: false,
      model: "",
      serial: "",
      make: "",
      fault: "",
      description: "",
      
      courtesy: "",
      bond: 0,
      service: 85,
      total: 85,
      gst: 0,
      tgst: 85
    };

    this.handleInputChange = this.handleInputChange.bind(this);
       
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <form>
        
        <br />Customer Type: *
        <label>
          <input
            name="consumer"
            type="checkbox"
            checked={this.state.consumer}
            onChange={this.handleInputChange} />Consumer  
        </label>
        <label>
          <input
            name="business"
            type="checkbox"
            checked={this.state.business}
            onChange={this.handleInputChange} />Business  
        </label>
        <br />
        <label>
          Title: *
          <select value={this.state.gender} onChange={this.handleChange}>
            <option value="">Select..</option>
            <option value="Dell XP"> Dell XP</option>
            <option value="HP Pavilion"> HP Pavilion</option>
            <option value="Macbook Pro"> Apple MacBook Pro</option>
            <option value="Charger"> Charger </option>
          </select>
        </label>
        <br />
        <label>
          First Name: *
          <input
            name="firstname"
            type="text"
            value={this.state.firstname}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Last Name: *
          <input
            name="lastname"
            type="text"
            value={this.state.lastname}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Street: *
          <input
            name="street"
            type="text"
            value={this.state.street}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Suburb: *
          <input
            name="suburb"
            type="text"
            value={this.state.suburb}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          City: *
          <input
            name="city"
            type="text"
            value={this.state.city}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Postcode: *
          <input
            name="postcode"
            type="text"
            value={this.state.postcode}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Phone: *
          <input
            name="phone"
            type="text"
            value={this.state.phone}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Email: *
          <input
            name="email"
            type="text"
            value={this.state.email}
            onChange={this.handleInputChange} />
        </label>
        <br />

        <br />
        <label>
          Purchase Date: *
          <input
            name="purchase"
            type="date"
            value={this.state.purchase}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Repair Date: *
          <input
            name="repair"
            type="date"
            value={this.state.repair}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Under Warranty: *
          <input
            name="warranty"
            type="checkbox"
            checked={this.state.warranty}
            onChange={this.handleInputChange} />  
        </label>
        <br />
        <label>
          Serial Number: *
          <input
            name="serial"
            type="text"
            value={this.state.serial}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Make: *
          <select value={this.state.make} onChange={this.handleChange}>
            <option value="">Select..</option>
            <option value="Dell">Dell</option>
            <option value="Apple">Apple</option>
            <option value="HP">HP</option>
            <option value="Acer">Acer</option>
            <option value="Lenovo">Lenovo</option>
            <option value="Toshiba">Toshiba</option>
            <option value="Other">Other</option>
          </select>
        </label>
        <br />
        <label>
          Model Number: *
          <input
            name="model"
            type="text"
            value={this.state.model}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Fault Category: *
          <select value={this.state.fault} onChange={this.handleChange}>
            <option value="">Select..</option>
            <option value="Battery">Battery</option>
            <option value="Charging">Charging</option>
            <option value="Screen">Screen</option>
            <option value="SD-storage">SD-storage</option>
            <option value="Memory">Memory</option>
            <option value="Software">Software</option>
            <option value="Other">Other</option>
          </select>
        </label>
        <br />
        <label>
          Description: *
          <textarea 
            value={this.state.description} 
            onChange={this.handleChange} />
        </label>
        <br />

        <br />
        <label>
          <select value={this.state.courtesy} onChange={this.handleChange}>
            <option value="">Select..</option>
            <option value="Dell XP">Dell XP</option>
            <option value="Apple Macbook Pro">Apple Macbook Pro</option>
            <option value="HP Pavilion">HP Pavilion</option>
            <option value="Charger">Charger</option>
          </select>
        </label>
        <br />

        <br />
        <label>
          Bond: 
          <input
            name="bond"
            type="text"
            value={this.state.bond}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Service Fee: 
          <input
            name="service"
            type="text"
            value={this.state.service}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Total: 
          <input
            name="total"
            type="text"
            value={this.state.total}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          GST: 
          <input
            name="gst"
            type="text"
            value={this.state.gst}
            onChange={this.handleInputChange} />
        </label>
        <br />
        <label>
          Total(+GST): 
          <input
            name="tgst"
            type="text"
            value={this.state.tgst}
            onChange={this.handleInputChange} />
        </label>
      </form>
    );
  }
}


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<Booking />);