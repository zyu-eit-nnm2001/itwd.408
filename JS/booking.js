//----Customer line 1----
var nm0=document.getElementById("gender");
var nm1=document.getElementById('firstname');
var nm2=document.getElementById('lastname');
var nm = nm0+" "+nm1+" "+nm2;
//----Customer line 2----
var adr1=document.getElementById('street');
//----Customer line 3----`
var adr2=document.getElementById('suburb');
var adr3=document.getElementById('city');
var pcode=document.getElementById('postcode');
var adr = adr2+", "+adr3+", "+pcode;
//----Customer line 4----
var pnum=document.getElementById('phone-number');
//----Customer line 5----
var eml=document.getElementById('email');

//----Repair line 1----
var cusC=document.getElementById('customer');
var cusB=document.getElementById('business');
var cus;
if (cusC==True){cus="Customer";}
else if (cusB==True){cus="Business";}
//----Repair line 2----
let currentDate = new Date();
let cDay = currentDate.getDate();
let cMonth = currentDate.getMonth() + 1;
let cYear = currentDate.getFullYear();
let cTime = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
console.log(time);
var idate=cMonth+"/"+cDay+"/"+cYear+", "+cTime;
//----Repair line 3----
var nDay = cDay+5
var ndate=cMonth+"/"+nDay+"/"+cYear;

//----Detail line 1----
var pdate=document.getElementById('purchase');
//----Detail line 2----
var rdate=document.getElementById('repair');
//----Detail line 3----
var wrty=document.getElementById('warranty');
if (wrty==True){wrty="Yes ✓";}
else {wrty="No ✕";}
//----Detail line 4----
var mdn=document.getElementById('model');
//----Detail line 5----
var sen=document.getElementById('serial');
//----Detail line 6----
var mk=document.getElementById('make');
//----Detail line 7----
var flt=document.getElementById('fault');
//----Detail line 8----
var des=document.getElementById('description');



//----------------------------------------------------

//----Totals----
var bd;
//----when press the add button-----
function calBond(){}
if (cusB==True){bd=0;}

var svc;
if (wrty!=True){svc=85;}
else{svc=0;}
var ttl=bd+svc
var gst=ttl*0.15
var tgst=ttl*1.15




function check(){
  if($('input').val()==''){
      alert('Input cannot be empty')
      return false
  }
}

let itemObject = {phoneAdded: false, type: "", chargerAdded: false};    
//Declare a class: BookingClass
class BookingClass {
  //Constructor & Properties
  constructor() {
    //Properties
    this.customerType = "";
    this.name = "";
    this.email = "";
    this.phone = "";
    this.gender = "";
    this.purchaseDate = "";
    this.repairDate = "";
    this.warranty = true;
    this.courtesy_phone = "";
    this.phone_bond = 0;        
    this.charger_bond = 30;
    this.bond = 30;
    this.service_fee = 85;      
  }         

  //Methods
  updateBond() {
    if (this.customerType == "business") {
      this.bond = 0;
    } else {
      this.bond = this.phone_bond + this.charger_bond;
    }       
  }

  updateServiceFee() {        
    if (this.warranty == false) {
      this.service_fee = 85;
    } else  {
      this.service_fee = 0;
    }       
  }
}
let yourBooking = new BookingClass();    
$(document).ready(function() {
  $('legend').addClass("legend-style");
  $('input#purchaseDate').change(function() {
    let purchaseDate = new Date($(this).val());
    let today = new Date();
    if (purchaseDate.getTime() - today.getTime() > 0) {
      //alert("Purchase date must be before today!");
      $('input#purchaseDate').after('<p class="error-message">Purchase date must be before today!</p>');
      $(this).val("");//Reset value
    } else {
      $('p.error-message').remove();//Remove error message
    }
  });
  $('input#repairDate').change(function() {
    let purchaseDate = new Date($('input#purchaseDate').val());
    let repairDate = new  Date($("input#repairDate").val());
    let today = new Date();
    if (repairDate.getTime() - today.getTime() < 0) { 
      //alert("Repair date must be after today!");
      $('input#repairDate').after('<p class="error-message">Repair date must be after today!</p>');
      $(this).val("");//Reset value       
    } else {
      $('p.error-message').remove();//Remove error message
      //Check if warranty is checked or un-checked
      if ((repairDate.getTime() - purchaseDate.getTime())/(1000*360*24) < 2*365) {
        //alert("CHECKED");
        $('input#warranty').prop("checked", true);
      } else {
        //alert("UN-CHECKED");
        $('input#warranty').prop("checked", false);
      }         
    }       
  });


//-----------------------ADD REMOVE--------------------------
function addI(){

    if($("#itemType").val()=="HP Pavilion"){$("#itemTable").append("<tr>" + 
                            "<td>" + $("#itemType").val()+ "</td>" 
                            +"<td> 575$ </td>"
                            +"<td><button> Remove </button> </td>"
                          + "</tr>"); }
    if($("#itemType").val()=="Charger"){
      $("#itemTable").append("<tr>" + 
                            "<td>" + $("#itemType").val()+ "</td>" 
                            +"<td> 30$ </td>"
                            +"<td><button> Remove </button> </td>"
                          + "</tr>");
    }
    if($("#itemType").val()=="Dell XP"){
      $("#itemTable").append("<tr>" + 
                            "<td>" + $("#itemType").val()+ "</td>" 
                            +"<td> 400$ </td>"
                            +"<td><button> Remove </button> </td>"
                          + "</tr>");
      }
      if($("#itemType").val()=="Apple MacBook Pro"){
      $("#itemTable").append("<tr>" + 
                            "<td>" + $("#itemType").val()+ "</td>" 
                            +"<td> 400$ </td>"
                            +"<td><button> Remove </button> </td>"
                          + "</tr>");
      }
  
  };

  $("#itemTable").on("click", "td button", function(){
    
    $(this).closest("tr").remove();
  });


});


function formReset(){
  document.getElementById('customer').checked=false
  document.getElementById('business').checked=false
  document.getElementById("gender").value=''
  document.getElementById('firstname').value=''
  document.getElementById('lastname').value=''
  document.getElementById('street').value=''
  document.getElementById('suburb').value=''
  document.getElementById('city').value=''
  document.getElementById('postcode').value=''
  document.getElementById('phone-number').value=''
  document.getElementById('email').value=''

  document.getElementById('purchase').value=''
  document.getElementById('repair').value=''
  document.getElementById('warranty').checked=false
  document.getElementById('model').value=''
  document.getElementById('serial').value=''
  document.getElementById('make').value=''
  document.getElementById('fault').value=''
  document.getElementById('description').value=''

  document.getElementById('bond').value=''
  document.getElementById('service').value=''
  document.getElementById('total').value=''
  document.getElementById('gst').value=''
  document.getElementById('tgst').value=''
}


  






